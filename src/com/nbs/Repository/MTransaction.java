package com.nbs.Repository;

import java.time.LocalDateTime;

public class MTransaction {
	String bdxName;
	String bdxBranch;
	String transactionDate;
	String clerkUsername;
	String clientName;
	String clientNationalId;
	String clientGender;
	String clientPhysicalAddress;
	String street;
	String suburb;
	String city;
	String district;
	String currencyCode;
	String sourceCountryCode;
	String internationalPartnerCode;
	String sourceOfFundsCode;
	String transactionPurposeCode;
	String transferMode;
	String amount;
	public String getBdxName() {
		return bdxName;
	}
	public void setBdxName(String bdxName) {
		this.bdxName = bdxName;
	}
	public String getBdxBranch() {
		return bdxBranch;
	}
	public void setBdxBranch(String bdxBranch) {
		this.bdxBranch = bdxBranch;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getClerkUsername() {
		return clerkUsername;
	}
	public void setClerkUsername(String clerkUsername) {
		this.clerkUsername = clerkUsername;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientNationalId() {
		return clientNationalId;
	}
	public void setClientNationalId(String clientNationalId) {
		this.clientNationalId = clientNationalId;
	}
	public String getClientGender() {
		return clientGender;
	}
	public void setClientGender(String clientGender) {
		this.clientGender = clientGender;
	}
	public String getClientPhysicalAddress() {
		return clientPhysicalAddress;
	}
	public void setClientPhysicalAddress(String clientPhysicalAddress) {
		this.clientPhysicalAddress = clientPhysicalAddress;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getSuburb() {
		return suburb;
	}
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getSourceCountryCode() {
		return sourceCountryCode;
	}
	public void setSourceCountryCode(String sourceCountryCode) {
		this.sourceCountryCode = sourceCountryCode;
	}
	public String getInternationalPartnerCode() {
		return internationalPartnerCode;
	}
	public void setInternationalPartnerCode(String internationalPartnerCode) {
		this.internationalPartnerCode = internationalPartnerCode;
	}
	public String getSourceOfFundsCode() {
		return sourceOfFundsCode;
	}
	public void setSourceOfFundsCode(String sourceOfFundsCode) {
		this.sourceOfFundsCode = sourceOfFundsCode;
	}
	public String getTransactionPurposeCode() {
		return transactionPurposeCode;
	}
	public void setTransactionPurposeCode(String transactionPurposeCode) {
		this.transactionPurposeCode = transactionPurposeCode;
	}
	public String getTransferMode() {
		return transferMode;
	}
	public void setTransferMode(String transferMode) {
		this.transferMode = transferMode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}

