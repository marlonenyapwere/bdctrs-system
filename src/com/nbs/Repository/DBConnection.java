package com.nbs.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class DBConnection {

	public static Connection getDBConnection() {

		Connection dbConnection = null;
		String jdbcClassName = "com.ibm.as400.access.AS400JDBCDriver";
		String url = "jdbc:as400://192.168.1.33/B2059bbw";
		String user = "CSCP";
		String password = "CSCP";

		try {
			Class.forName(jdbcClassName);
			dbConnection = DriverManager.getConnection(url, user, password);
			System.out.println("connection successful...");
			return dbConnection;
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;

	}

}
