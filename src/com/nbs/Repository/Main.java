package com.nbs.Repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import com.google.gson.Gson;

public class Main {

	public static void main(String[] args) {
		Connection conn = DBConnection.getDBConnection();

		try {

			Statement statement = conn.createStatement();
			String s = "SELECT * FROM kfilnbs.sapf";

			ResultSet rs = statement.executeQuery(s);

			while (rs.next()) {

				MTransaction trans = new MTransaction();
				double balance = rs.getDouble("saama");
				String tDate = rs.getString("satstp");
				String branchName = rs.getString("saab");
				String transactionPurposeCode = rs.getString("satcd");
				
				System.out.println("Balance :" + balance);
				System.out.println("Date :" + tDate);
				System.out.println("Branch :" + branchName);
				System.out.println("transactionPurposeCode :" + transactionPurposeCode);
				
				
				trans.setBdxName("");
				trans.setBdxBranch("");
				trans.setTransactionDate("");
				trans.setClerkUsername("");
				trans.setClientName("");
				trans.setClientNationalId("");
				trans.setClientGender("");
				trans.setClientPhysicalAddress("");
				trans.setStreet("");
				trans.setSuburb("");
				trans.setCity("");
				trans.setDistrict("");
				trans.setCurrencyCode("");
				trans.setInternationalPartnerCode("");
				trans.setSourceOfFundsCode("");
				trans.setTransactionPurposeCode("");
				trans.setTransferMode("");
				trans.setAmount("");

				Gson gsonMapper = new Gson();

				System.out.println(gsonMapper.toJson(trans));
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
